/**
 * Resolves a type name from a string, an object instance, a named function, or a type (constructor function).
 * @param o The object from which to resolve the type name.
 * @returns The type name if it was able to be resolve, otherwise `undefined`.
 */
export function resolveTypeName(
    o: object | string | undefined
): string | undefined {
    if (o == undefined) return o;
    if (typeof o === "string") return o;
    if (typeof o === "function") return o.name ? o.name : '[anonymous function]';

    return o.constructor != undefined ? o.constructor.name : undefined;
}

export function genericTyped(type: object | string | undefined, message: string): string {
    return `${resolveTypeName(type)}: ${message}`;
}

export function argument(argumentName: string, message?: string): string {
    return concat(`The argument "${argumentName}" is invalid.`, message);
}

export function argumentNull(argumentName?: string): string {
    return argumentName != undefined
        ? `The argument "${argumentName}" cannot be null.`
        : "Argument null.";
}

export function invalidOperation(message?: string): string {
    return message != undefined
        ? `Invalid Operation: ${message}`
        : "Operation is not valid due to the current state of the object.";
}

export function invalidOpBeforeMethod(
    methodName: string,
    className?: object | string
): string {
    return invalidOperation(
        `The operation is invalid before the "${formatMethodName(
            methodName,
            className
        )}" method has been called.`
    );
}

export function invalidOpAfterMethod(
    methodName: string,
    className?: object | string
): string {
    return invalidOperation(
        `The operation is invalid after the "${formatMethodName(
            methodName,
            className
        )}" method has been called.`
    );
}

export function invalidOpOutsideState(
    stateName: string,
    context?: object | string
): string {
    return invalidOperation(
        `The operation is invalid outside of the ${formatStateName(
            stateName,
            context
        )} state.`
    );
}

export function invalidOpDuringState(
    stateName: string,
    context?: object | string
): string {
    return invalidOperation(
        `The operation is invalid during the ${formatStateName(
            stateName,
            context
        )} state.`
    );
}

export function invalidOpRequiresTarget(
    targetName: string,
    context?: object | string
): string {
    return invalidOperation(
        `The operation may only be performed on the ${formatTargetName(
            targetName,
            context
        )}.`
    );
}

export function invalidOpRequiresTargetAny(
    targetName: string,
    context?: object | string
): string {
    return invalidOperation(
        `The operation may only be performed on a ${formatTargetName(
            targetName,
            context
        )}.`
    );
}

export function objectDisposed(): string {
    return invalidOperation("Object disposed.");
}

function concat(prefix: string, message?: string): string {
    return message != undefined ? `${prefix} ${message}` : prefix;
}

function formatMethodName(
    methodName: string,
    className: object | string | undefined
): string {
    return className != undefined
        ? `${resolveTypeName(className)}.${methodName}`
        : methodName;
}

function formatStateName(
    stateName: string,
    className: object | string | undefined
): string {
    return className != undefined
        ? `${resolveTypeName(className)} ${stateName}`
        : stateName;
}

function formatTargetName(
    targetName: string,
    context: object | string | undefined
): string {
    return context != undefined
        ? `${targetName} ${resolveTypeName(context)}`
        : targetName;
}
