import { argument } from "../src";

describe("argumentMessage", () => {
    it("Should display standard message with arg param", () => {
        expect(argument("param")).toBe('The argument "param" is invalid.');
    });

    it("Should append extra message", () => {
        expect(argument("param", "Extra")).toBe(
            'The argument "param" is invalid. Extra'
        );
    });
});
