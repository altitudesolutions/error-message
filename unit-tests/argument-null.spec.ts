import { argumentNull } from "../src";

describe("argumentNullMessage", () => {
    it("Should default with no params", () => {
        expect(argumentNull()).toBe("Argument null.");
    });

    it("Should display standard message with arg param", () => {
        expect(argumentNull("param")).toBe(
            'The argument "param" cannot be null.'
        );
    });
});
