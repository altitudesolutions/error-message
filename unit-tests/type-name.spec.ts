import { resolveTypeName, genericTyped } from "../src";

// tslint:disable-next-line: no-unnecessary-class
class TestClass {
    static typeName(): string | undefined {
        return resolveTypeName(TestClass);
    }
}

describe("resolveTypeName", () => {
    function testFunction(): void { }

    it("Should resolve generic Object name", () => {
        expect(resolveTypeName({})).toBe("Object");
    });

    it("Should resolve from a string", () => {
        expect(resolveTypeName("StringName")).toBe("StringName");
    });

    it("Should resolve from an instance", () => {
        expect(resolveTypeName(new TestClass())).toBe("TestClass");
    });

    it("Should resolve from a type", () => {
        expect(resolveTypeName(TestClass)).toBe("TestClass");
        expect(TestClass.typeName()).toBe("TestClass");
        expect(resolveTypeName(Number)).toBe("Number");
        expect(resolveTypeName(Date)).toBe("Date");
        expect(resolveTypeName(String)).toBe("String");
    });

    it("Should resolve from a named function", () => {
        expect(resolveTypeName(testFunction)).toBe("testFunction");
    });

    it("Should resolve from an anonymous function", () => {
        expect(resolveTypeName(() => { })).toBe("[anonymous function]");
    });

    it("Should display generic message", () => {
        expect(genericTyped(TestClass, 'test message')).toBe("TestClass: test message");
    });
});
