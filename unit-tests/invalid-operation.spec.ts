import * as Message from "../src";

// tslint:disable-next-line: no-unnecessary-class
class TestClass {}

const TEST_INSTANCE = new TestClass();

describe("invalidOperationMessage", () => {
    it("Should default with no params", () => {
        expect(Message.invalidOperation()).toBe(
            "Operation is not valid due to the current state of the object."
        );
    });

    it("Should display custom message", () => {
        expect(Message.invalidOperation("Custom")).toBe(
            "Invalid Operation: Custom"
        );
    });

    it("Should display invalid operation before method without class name", () => {
        expect(Message.invalidOpBeforeMethod("methodName")).toBe(
            `Invalid Operation: The operation is invalid before the "methodName" method has been called.`
        );
    });

    it("Should display invalid operation before method with class name", () => {
        expect(Message.invalidOpBeforeMethod("methodName", "ClassName")).toBe(
            `Invalid Operation: The operation is invalid before the "ClassName.methodName" method has been called.`
        );
    });

    it("Should display invalid operation before method with object name", () => {
        expect(Message.invalidOpBeforeMethod("methodName", TEST_INSTANCE)).toBe(
            `Invalid Operation: The operation is invalid before the "TestClass.methodName" method has been called.`
        );
    });

    it("Should display invalid operation after method without class name", () => {
        expect(Message.invalidOpAfterMethod("methodName")).toBe(
            `Invalid Operation: The operation is invalid after the "methodName" method has been called.`
        );
    });

    it("Should display invalid operation after method with class name", () => {
        expect(Message.invalidOpAfterMethod("methodName", "ClassName")).toBe(
            `Invalid Operation: The operation is invalid after the "ClassName.methodName" method has been called.`
        );
    });

    it("Should display invalid operation after method with object name", () => {
        expect(Message.invalidOpAfterMethod("methodName", TEST_INSTANCE)).toBe(
            `Invalid Operation: The operation is invalid after the "TestClass.methodName" method has been called.`
        );
    });

    it("Should display invalid operation outside of state without context name", () => {
        expect(Message.invalidOpOutsideState("stateName")).toBe(
            `Invalid Operation: The operation is invalid outside of the stateName state.`
        );
    });

    it("Should display invalid operation outside of state with context name", () => {
        expect(Message.invalidOpOutsideState("stateName", "Context")).toBe(
            `Invalid Operation: The operation is invalid outside of the Context stateName state.`
        );
    });

    it("Should display invalid operation outside of state with object context name", () => {
        expect(Message.invalidOpOutsideState("stateName", TEST_INSTANCE)).toBe(
            `Invalid Operation: The operation is invalid outside of the TestClass stateName state.`
        );
    });

    it("Should display invalid operation during state without context name", () => {
        expect(Message.invalidOpDuringState("stateName")).toBe(
            `Invalid Operation: The operation is invalid during the stateName state.`
        );
    });

    it("Should display invalid operation during state with context name", () => {
        expect(Message.invalidOpDuringState("stateName", "Context")).toBe(
            `Invalid Operation: The operation is invalid during the Context stateName state.`
        );
    });

    it("Should display invalid operation during state with object context name", () => {
        expect(Message.invalidOpDuringState("stateName", TEST_INSTANCE)).toBe(
            `Invalid Operation: The operation is invalid during the TestClass stateName state.`
        );
    });

    it("Should display invalid operation requires target without context name", () => {
        expect(Message.invalidOpRequiresTarget("targetName")).toBe(
            `Invalid Operation: The operation may only be performed on the targetName.`
        );
    });

    it("Should display invalid operation outside of state with context name", () => {
        expect(Message.invalidOpRequiresTarget("targetName", "Context")).toBe(
            `Invalid Operation: The operation may only be performed on the targetName Context.`
        );
    });

    it("Should display invalid operation outside of state with object context name", () => {
        expect(
            Message.invalidOpRequiresTarget("targetName", TEST_INSTANCE)
        ).toBe(
            `Invalid Operation: The operation may only be performed on the targetName TestClass.`
        );
    });

    it("Should display invalid operation during state without context name", () => {
        expect(Message.invalidOpRequiresTargetAny("targetName")).toBe(
            `Invalid Operation: The operation may only be performed on a targetName.`
        );
    });

    it("Should display invalid operation during state with context name", () => {
        expect(
            Message.invalidOpRequiresTargetAny("targetName", "Context")
        ).toBe(
            `Invalid Operation: The operation may only be performed on a targetName Context.`
        );
    });

    it("Should display invalid operation during state with object context name", () => {
        expect(
            Message.invalidOpRequiresTargetAny("targetName", TEST_INSTANCE)
        ).toBe(
            `Invalid Operation: The operation may only be performed on a targetName TestClass.`
        );
    });
});
